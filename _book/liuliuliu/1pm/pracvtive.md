上课的步骤，连接的工具图片，出现的问题，如何解决的。

# The basic steps

## 1.download required software.<font face="" color=blue size=4>
* [Pic go]
* Git
* Visual Studio Code
* Node.js

## 2.commissioning
* <font face="" color=blue size=4>Pic go
  <font face="" color=black size=3>
  * Insert plug-in components
  ![](https://gitlab.com/pic-01/pic-liu/uploads/9599eafeaa057b278748d467f0c45e62/picgo.png)
  * settings
   ![](https://gitlab.com/pic-01/pic-liu/uploads/9c2a7329accd390648ce27219f0f7cb4/pic图床设置.png)
  * New group
   ![](https://gitlab.com/pic-01/pic-liu/uploads/c414f2465778cff59a39ad2aca7b1c82/new_group.png)
   ①
   ![](https://gitlab.com/pic-01/pic-liu/uploads/ce131db0add75b247655b9be2958292f/_.png)
   ②
   ![](https://gitlab.com/pic-01/pic-liu/uploads/363a608dd9f87b3727c9ff7a9c3652f8/_.png)
   ![](https://gitlab.com/pic-01/pic-liu/uploads/dba8f1f7d6d9fe7a44da502eff09fdb2/_后续.png)
   ③
   ![](https://gitlab.com/pic-01/pic-liu/uploads/1458d4b466e1833e42f70e31839ca4a3/_.png)
   * Back Picgo. 
   * Enter the imformation.Click on "设为默认图床” and “确定”.
   * Now you can upload the pictures you need.
* <font face="" color=blue size=4>Git
  <font face="" color=red size=3>
  Preparation Phase
    * Create a new project and create a new file.
  ![](https://gitlab.com/pic-01/pic-liu/uploads/9be68ceb52000875ccbde8b1088fdd0b/gitlab1.png)
  ![](https://gitlab.com/pic-01/pic-liu/uploads/68d2c4e477f5f8ccebe9fc58e377fe28/gitlab2.png)<font face="" color=black size=3>
  * 1.Initialization
     ```
    $ cd /c/Users/95389/Documents/test
    $ git init 
    Initialized empty Git repository in C:/Users/95389/Documents/test/.git/
    ```
  * 2.Configure,setting up local identity 
    ```
    $ git config user.name "XXX"  
    $ git config user.email "XXX@xx.com":
    ```
  * 3.Clone
    ```
    $ git clone git@gitlab.com:nex-fab/guide-book.git "your url"
    ```
    ![](https://gitlab.com/pic-01/pic-liu/uploads/da275fe46be279737fbb1b4c04234c54/clone.png)

    * If Ok , we will find the following information
    ```
    wait...
    ```
    * But if we get the following
    ```
    fatal: could not create leading directories of 'git@gitlab.com:dd_ddd/test.git': Invalid argument
    ```
    <font face="" color=black size=3>  or

    ```
    fatal: Could not read from remote repository.
    Please make sure you have the correct access rightsand the repository exists.
    ```
    * The problem is SSH key，check if we have generate SSH.
    ```
    $ ls ~/.ssh/
    ```
    * There will be two result: Result1: we have the following information.
    ```
    $ id_rsa id_rsa.pub known_hosts
    ```
    * we can skip "generate SSH" Result2: no above information, we should "generate SSH"
    ```
    $ ssh-keygen -t rsa -C  "953896560@qq.com"
    Generating public/private rsa key pair.
    Enter file in which to save the key (/c/Users/95389/.ssh/id_rsa):
    /c/Users/95389/.ssh/id_rsa already exists.
    Overwrite (y/n)? y
    Enter passphrase (empty for no passphrase):
    Enter same passphrase again:
    Your identification has been saved in /c/Users/95389/.ssh/id_rsa
    Your public key has been saved in /c/Users/95389/.ssh/id_rsa.pub
    The key fingerprint is:
    SHA256:bXutx1HRQzboaK8cu4VYovxBHnaHTA/Jqr3ux8Zz1qw 953896560@qq.com
    The key's randomart image is:
    +---[RSA 3072]----+
    |              o+.|
    |          . ...oo|
    |           =o   o|
    |         .+o+.  .|
    |        S*+=.o . |
    |      . B.*ooo.  |
    |       + ==.=o+. |
    |        . o@.+oo |
    |        o=o.*E.  |
    +----[SHA256]-----+
    ```
    * Check the SSH key and copy to gitlab
    ```
     $ cat ~/.ssh/id_rsa.pub
    ```
    ```
    $ ssh-rsa AAAAB....=your email
     ```
     ![](https://gitlab.com/pic-01/pic-liu/uploads/36e345d882ddb6c9e3d94c6376de6624/settings.png)
     then
     ![](https://gitlab.com/pic-01/pic-liu/uploads/b443a907aadf217a6cc16c5104acff8c/paste_ssh.png)

    * git clone again
    ```
    $ git clone git@gitlab.com:dd_ddd/test.git
    Cloning into 'test'...
    remote: Enumerating objects: 9, done.
    remote: Counting objects: 100% (9/9), done.
    remote: Compressing objects: 100% (6/6), done.
    remote: Total 9 (delta 1), reused 0 (delta 0), pack-reused 0
    Receiving objects: 100% (9/9), done.
    Resolving deltas: 100% (1/1), done.
    ```
    * Now you can get data to your local machine
    ![](https://gitlab.com/pic-01/pic-liu/uploads/5abaeb3548fa7fba3537181c1d953d5e/clone_到本地.png)
    
    * Modify SUMMARY files on the gitlab————"hello world 111"
  * Command
    * git pull : download gitlab to local
    ```
    $ git pull origin master
    fatal: 'origin' does not appear to be a git repository
    fatal: Could not read from remote repository.

    Please make sure you have the correct access rights and the repository exists.
    ```
    * If you encounter the above situation.
    * solution : we need connect remote as origin
    ```
    git remote add origin "your url"
    ```
    ```
    $ git pull origin master
    remote: Enumerating objects: 12, done.
    remote: Counting objects: 100% (12/12), done.
    remote: Compressing objects: 100% (8/8), done.
    remote: Total 12 (delta 2), reused 0 (delta 0), pack-reused 0
    Unpacking objects: 100% (12/12), 1.24 KiB | 7.00 KiB/s, done.
    From gitlab.com:dd_ddd/test
     * branch            master     -> FETCH_HEAD
     * [new branch]      master     -> origin/master
    ```
    * Now the SUMMARY file is changed.
    ![](https://gitlab.com/pic-01/pic-liu/uploads/6861e4bdbd20f2820df7871e2e024457/Git_pull.png)
    * The same, we can modify the local SUMMARY to achieve the effect  SUMMARY of the website
    ![](https://gitlab.com/pic-01/pic-liu/uploads/bd1a6f9c15d3eee66fbfc1d18eb0f792/修改本地SUMMARY.png)
    * git push
    ```
    fatal: The current branch master has no upstream branch.
    To push the current branch and set the remote as upstream, use

    git push --set-upstream origin master
    ```
    ```
    $ git add --all
    ```
    ```
    $ git commit -m "code"
    [master 12af0f1] code
    2 files changed, 2 insertions(+), 1 deletion(-)
    create mode 160000 test
    ```
    ```
    git push origin master
    Enumerating objects: 5, done.
    Counting objects: 100% (5/5), done.
    Delta compression using up to 8 threads
    Compressing objects: 100% (2/2), done.
    Writing objects: 100% (3/3), 346 bytes | 173.00 KiB/s, done.
    Total 3 (delta 0), reused 0 (delta 0)
    To gitlab.com:dd_ddd/test.git
    3583677..12af0f1  master -> master
    ```
    ![](https://gitlab.com/pic-01/pic-liu/uploads/1aa1c5f02ce7bae68ae2feee8f55ac2f/SUMMARY.png)
* <font face="" color=blue size=4>Visual Studio Code
<font face="" color=black size=3>
![](https://gitlab.com/pic-01/pic-liu/uploads/55f4baa5f0779cc0dbd26b1d6c8d89ef/TXT-MARKDOWN.jpg)
![](https://gitlab.com/pic-01/pic-liu/uploads/018fa63e303a4da2cf02735fbfa2ce74/vscode_预览.jpg)
![](https://gitlab.com/pic-01/pic-liu/uploads/2bc087bc067959b80256f7926c606bd4/preview.jpg)

  * On the edit page

| name          | Are                 | 
| ------------- |:-------------------:| 
| #             | first level title   | 	
| ##            | Secondary title     | 
|*....          | list                |
| * ...*        | Italic              |
| ** .....**    | Bold                | 
```
[这是baidu](www.baidu.com)
![这是图片](https://goss1.cfp.cn/creative/vcg/800/version23/VCG41649681841.jpg)
<font face="华文彩云" color=red size=5>字体是“华文彩云”，颜色是红色，字号是5
```
[这是baidu](www.baidu.com)
![这是图片](https://cdn2.hubspot.net/hubfs/3027780/coronabanner.png)
<font face="华文彩云" color=red size=5>字体是“华文彩云”，颜色是红色，字号是5
<font face="默认" color=blue size=4>
* node.js
  <font face="" color=black size=3>
```
$ node -v
v12.16.1
$ sudo npm install -g gitbook-cli
Password:
/usr/local/bin/gitbook -> /usr/local/lib/node_modules/gitbook-cli/bin/gitbook.js
+ gitbook-cli@2.3.2
added 578 packages from 672 contributors in 31.08s
$ gitbook -V
CLI version: 2.3.2
Installing GitBook 3.2.3
xcode-select: error: tool 'xcodebuild' requires Xcode, but active developer directory '/Library/Developer/CommandLineTools' is a command line tools instance

xcode-select: error: tool 'xcodebuild' requires Xcode, but active developer directory '/Library/Developer/CommandLineTools' is a command line tools instance

No receipt for 'com.apple.pkg.CLTools_Executables' found at '/'.

No receipt for 'com.apple.pkg.DeveloperToolsCLILeo' found at '/'.

No receipt for 'com.apple.pkg.DeveloperToolsCLI' found at '/'.

gyp: No Xcode or CLT version detected!
xcode-select: error: tool 'xcodebuild' requires Xcode, but active developer directory '/Library/Developer/CommandLineTools' is a command line tools instance

xcode-select: error: tool 'xcodebuild' requires Xcode, but active developer directory '/Library/Developer/CommandLineTools' is a command line tools instance

No receipt for 'com.apple.pkg.CLTools_Executables' found at '/'.

No receipt for 'com.apple.pkg.DeveloperToolsCLILeo' found at '/'.

No receipt for 'com.apple.pkg.DeveloperToolsCLI' found at '/'.

gyp: No Xcode or CLT version detected!
gitbook@3.2.3 ../../var/folders/1y/wr1qt4y12hjfxy6przpsq00r0000gn/T/tmp-3923f5PAt13AC8XP/node_modules/gitbook
├── escape-string-regexp@1.0.5
├── escape-html@1.0.3
├── destroy@1.0.4
├── ignore@3.1.2
├── bash-color@0.0.4
├── gitbook-plugin-livereload@0.0.1
├── cp@0.2.0
├── graceful-fs@4.1.4
├── nunjucks-do@1.0.0
├── github-slugid@1.0.1
├── direction@0.1.5
├── q@1.4.1
├── spawn-cmd@0.0.2
├── gitbook-plugin-fontsettings@2.0.0
├── open@0.0.5
├── is@3.3.0
├── object-path@0.9.2
├── extend@3.0.2
├── json-schema-defaults@0.1.1
├── gitbook-plugin-search@2.2.1
├── jsonschema@1.1.0
├── crc@3.4.0
├── urijs@1.18.0
├── semver@5.1.0
├── immutable@3.8.2
├── front-matter@2.3.0
├── dom-serializer@0.1.0 (domelementtype@1.1.3, entities@1.1.2)
├── omit-keys@0.1.0 (array-difference@0.0.1, isobject@0.2.0)
├── error@7.0.2 (xtend@4.0.2, string-template@0.2.1)
├── npmi@2.0.1 (semver@4.3.6)
├── mkdirp@0.5.1 (minimist@0.0.8)
├── resolve@1.1.7
├── tmp@0.0.28 (os-tmpdir@1.0.2)
├── send@0.13.2 (statuses@1.2.1, range-parser@1.0.3, fresh@0.3.0, etag@1.7.0, ms@0.7.1, depd@1.1.2, mime@1.3.4, debug@2.2.0, http-errors@1.3.1, on-finished@2.3.0)
├── gitbook-plugin-theme-default@1.0.7
├── rmdir@1.2.0 (node.flow@1.2.3)
├── js-yaml@3.13.1 (esprima@4.0.1, argparse@1.0.10)
├── tiny-lr@0.2.1 (parseurl@1.3.3, livereload-js@2.4.0, qs@5.1.0, debug@2.2.0, faye-websocket@0.10.0, body-parser@1.14.2)
├── fresh-require@1.0.3 (is-require@0.0.1, shallow-copy@0.0.1, sleuth@0.1.1, astw@1.3.0, acorn@0.9.0, through2@0.6.5, escodegen@1.14.1)
├── gitbook-plugin-lunr@1.2.0 (html-entities@1.2.0, lunr@0.5.12)
├── gitbook-plugin-highlight@2.0.2 (highlight.js@9.2.0)
├── moment@2.13.0
├── gitbook-plugin-sharing@1.0.2 (lodash@3.10.1)
├── gitbook-markdown@1.3.2 (kramed-text-renderer@0.2.1, gitbook-html@1.3.3, kramed@0.5.6, lodash@4.17.15)
├── i18n-t@1.0.1 (lodash@4.17.15)
├── gitbook-asciidoc@1.2.2 (gitbook-html@1.3.3, asciidoctor.js@1.5.5-1, lodash@4.17.15)
├── npm@3.9.2
├── cheerio@0.20.0 (entities@1.1.2, css-select@1.2.0, lodash@4.17.15, htmlparser2@3.8.3, jsdom@7.2.2)
├── chokidar@1.5.0 (async-each@1.0.3, path-is-absolute@1.0.1, inherits@2.0.4, glob-parent@2.0.0, is-glob@2.0.1, is-binary-path@1.0.1, anymatch@1.3.2, readdirp@2.2.1)
├── nunjucks@2.5.2 (asap@2.0.6, yargs@3.32.0, chokidar@1.7.0)
├── read-installed@4.0.3 (debuglog@1.0.1, util-extend@1.0.3, slide@1.1.6, readdir-scoped-modules@1.1.0, read-package-json@2.1.1)
├── cpr@1.1.1 (rimraf@2.4.5)
├── request@2.72.0 (tunnel-agent@0.4.3, forever-agent@0.6.1, oauth-sign@0.8.2, aws-sign2@0.6.0, is-typedarray@1.0.0, caseless@0.11.0, stringstream@0.0.6, aws4@1.9.1, isstream@0.1.2, json-stringify-safe@5.0.1, tough-cookie@2.2.2, node-uuid@1.4.8, qs@6.1.2, combined-stream@1.0.8, mime-types@2.1.26, hawk@3.1.3, bl@1.1.2, form-data@1.0.1, http-signature@1.1.1, har-validator@2.0.6)
└── juice@2.0.0 (deep-extend@0.4.2, slick@1.12.2, batch@0.5.3, cssom@0.3.1, commander@2.9.0, cross-spawn-async@2.2.5, web-resource-inliner@2.0.0)
GitBook version: 3.2.3