#### 1.Find a simple picture
![](https://gitlab.com/pic-01/pic-liu/uploads/562d0a9c83e468e17d04c01ac8810c75/D07373A15D93E3710A7F108326DB0991.png)
#### 2.Import
![](https://gitlab.com/pic-01/pic-liu/uploads/9a2b7d63217cd1805f5f9df24e39ec23/9632A94C474D8208FEA0981FABD7329E.png)
#### 3.Sketch drawings
![](https://gitlab.com/pic-01/pic-liu/uploads/f415e17a9faf96f5e4786b493a916124/AEE7905B38AA1B571F6B891BD8C03CC7.png)
#### 4.Select the profile
![](https://gitlab.com/pic-01/pic-liu/uploads/967c118c33509bb4fb17cec0c75993d0/A92B005563364E4116BEA6C69EAEF390.png)
#### 5.Expend
![](https://gitlab.com/pic-01/pic-liu/uploads/750e1552853fb60e914a509b7445cb5e/F40B51A3AC23578D15B5A743800255D4.png)
#### 6.Ungroup
![](https://gitlab.com/pic-01/pic-liu/uploads/25a5c6a123804d90eaca9d8a2b2f7e60/9C6C2075D98E34BA73EFB5FA30AD9FBE.png)
#### 7.export
![](https://gitlab.com/pic-01/pic-liu/uploads/da852e93c018c7148a378d152af3fbf1/B1E22EDEFA9D1A685B450A2AAF608380.png)
#### 8.Choose "DWG""DXF" format
![](https://gitlab.com/pic-01/pic-liu/uploads/2cacfa2cf1dc78621801870e7761dfca/52C079B0509BA94772567BB6A9C94E1C.png)
#### 9.Use the "x" and "delete" command.
![](https://gitlab.com/pic-01/pic-liu/uploads/df5cefb349c6e4ce9a05afbfbc7d18a3/elephant.gif)