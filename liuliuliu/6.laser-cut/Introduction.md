
### History
* 1960——The earliest practical working laser——Finding solutions for use.
* 1964(Bell Labs)——Kumar Patel invented the CO2 mixture gas cutting process.
*  In the same year and the same place, the crystal laser technology was invented by J.E.Geusic.
* 1965——First use of laser cutting——Used in diamond mining.
* 1980——Twenty thousand commercial cutting machines have been installed all over the world——About $7.5 billion
* From 2000 to 2004, a complete set of high-power laser equipment was produced, which was used in cutting, welding, drilling and other fields
* 2009-2012,YAG Laser cutting muchine.
* 2015-2016,Disc laser cutting machine.
* 2012-now,Optical fiber metal laser cutting machine.

### Type
* According to the power can be divided into: high, medium and low power fiber laser cutting machine;
* According to cutting materials can be divided into: metal and non-metallic laser cutting machine;
* According to the type of processing can be divided into: CO2 laser cutting machine, YAG, fiber laser cutting machine
* According to the wavelength:
  * The  infrared 10600nm CO2 laser, and the main cutting uses include leather, cloth, wood, acrylic, etc.; 
  * The infrared 1064nm fiber laser is mainly used for metal cutting, such as carbon steel, stainless steel, aluminum alloy, copper, etc.; 
  * The 532nm band green laser is mainly used for cutting non-metallic materials such as glass; 
  * The 355nm band ultraviolet is used for cutting Laser, mainly used for non-metallic fine cutting, such as FPC, PI, pet, polymer materials, etc.
### Principle
* Laser is a kind of enhanced light obtained by stimulated radiation
* Features
  * High intensity and brightness
  * Good coherence. Long coherence length
  * Good directivity, almost parallel light  
  * The wavelength and frequency are determined and the monochromatic property is good

![](https://gitlab.com/pic-01/pic-liu/uploads/d04f29fe44fd070cd1d1898b55404f21/20200805150753.png)
![](https://gitlab.com/pic-01/pic-liu/uploads/a7e46ebf8834614e589565806147833b/20200805150726.png)
![](https://gitlab.com/pic-01/pic-liu/uploads/5e2f53c95e63e2575c2e3707282485ee/20200805150644.png)

### Advantage
  * Good cutting quality
    * The incision was thin and narrow
    * The cutting surface is smooth and beautiful, and the surface roughness is small
    * Small heat affected area, small workpiece deformation and high cutting accuracy
  * High cutting efficiency
  * It can work stably and continuously for a long time
  * cheap 
* Laser generator

### Test
![](https://gitlab.com/pic-01/pic-liu/uploads/ab185106ac470cb583113651d2f94dd2/20200805143816.png)
* 5mm Acrylic cutting test

![](https://gitlab.com/pic-01/pic-liu/uploads/d9c60255bf78c162e26d0c7666d47fa8/20200805145559.png)
![](https://gitlab.com/pic-01/pic-liu/uploads/97e04d067ef45bc02edce6fc3692ec1f/20200805150451.png)
![](https://gitlab.com/pic-01/pic-liu/uploads/e6b8c946bf5afef31dd59ad67acb9c60/20200805150523.png)

<center>5MM-TEST</center>

| Power\speed   | 10  |  15 |  20 |  25 |  30 |
| :-----------: |:---:|:---:|:---:|:---:|:---:| 
| 40            |  N  |  N  |  N  |  N  |  N  | 	
| 50            |  Y  |  N  |  N  |  N  |  N  | 	
| 60            |  Y  |  N  |  N  |  N  |  N  | 	
| 70            |  Y  |  N  |  N  |  N  |  N  | 	
| 80            |  Y  |  N  |  N  |  N  |  N  | 	


<center>3MM-TEST</center>

| Power\speed   | 20  |  25 |  30 |  35 |  40 |
| :-----------: |:---:|:---:|:---:|:---:|:---:| 
| 40            |  Y  |  N  |  N  |  N  |  N  | 	
| 50            |  Y  |  N  |  N  |  N  |  N  | 	
| 60            |  Y  |  Y  |  N  |  N  |  N  | 	
| 70            |  Y  |  Y  |  N  |  N  |  N  | 	
| 80            |  Y  |  Y  |  N  |  N  |  N  | 	

### Practive
![](https://gitlab.com/pic-01/pic-liu/uploads/763b82b9d6961970d553aa8b16fab8d2/20200805150835.png)
##### Select paper size
![](https://gitlab.com/pic-01/pic-liu/uploads/8093e6e53c85e3e0d3136cf8c12e8c87/20200805162059.png)
Preliminary engineering drawing
![](https://gitlab.com/pic-01/pic-liu/uploads/62713ea2aedff60b31fedd542682c38e/20200805162127.png)
Save
![](https://gitlab.com/pic-01/pic-liu/uploads/e30d06fb292c418398e6a354fb4bb179/20200805162202.png)
Use the "move" command to make the layout reasonable
![](https://gitlab.com/pic-01/pic-liu/uploads/90543571e3d758e6081e21f2da55e55e/20200805162313.png)
![](https://gitlab.com/pic-01/pic-liu/uploads/4fcda7b13358d787f1b4349a10d07757/20200805162347.png)
Save format.dxf
![](https://gitlab.com/pic-01/pic-liu/uploads/7e0a197b9bcb3dfc5eece35ae7904028/20200805162433.png)
Open the PowerCut
![](https://gitlab.com/pic-01/pic-liu/uploads/a7be2a17df220fe82ca4fc0649c5f4c3/20200805162740.png)
Choose the right speed and power
![](https://gitlab.com/pic-01/pic-liu/uploads/6a64f0ea971ef3e09117e3c94d0d90ef/20200805162821.png)
Connecting the machine
![](https://gitlab.com/pic-01/pic-liu/uploads/ea35b1b3b6e9cf922c96ce31f35488aa/20200805162922.png)
Load
![](https://gitlab.com/pic-01/pic-liu/uploads/10c4f880672c749e6c33974a7074f967/20200805162955.png)
The pattern will be displayed on the control interface of the machine
![](https://gitlab.com/pic-01/pic-liu/uploads/42cc73eca00f05a81234553c3dcf58c4/20200805163023.png)
Adjust the focus to 10 mm
![](https://gitlab.com/pic-01/pic-liu/uploads/dcbbd68208388a41e2e15dd452b09488/20200805163056.png)

Simulate cutting contour

![](https://gitlab.com/pic-01/pic-liu/uploads/a8db20b63cdcbbd2f4242ff8d108f289/2.gif)

Start

![](https://gitlab.com/pic-01/pic-liu/uploads/7015ad50ae7e99997f5447077e6383dd/8.gif)

Question 1:

![](https://gitlab.com/pic-01/pic-liu/uploads/72563f9e04db14cbf2ffd70a0c65f758/20200805163234.png)
Reason: the platform is not flat enough
Question 2:
![](https://gitlab.com/pic-01/pic-liu/uploads/732f08d15b89bee460b6adfffe09b1f5/20200805163350.png)
![](https://gitlab.com/pic-01/pic-liu/uploads/8c2ad7ac9ec105b04b6d7e4a6b0779f0/20200805163432.png)
Reason:The drawing scale is not 1:1
Assemble
![](https://gitlab.com/pic-01/pic-liu/uploads/d9f38dd5e41aec6539b1d32afd4912bc/20200805163503.png)
![](https://gitlab.com/pic-01/pic-liu/uploads/5aed8255d6651b2c54c9125e1fba00e5/20200805163536.png)