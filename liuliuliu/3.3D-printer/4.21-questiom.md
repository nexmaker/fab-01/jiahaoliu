* ## In the process of using, it is not smooth sailing.
  * ### Question 1
    * #### Material cannot be adsorbed on the support surface
        ![](https://gitlab.com/pic-01/pic-liu/uploads/b8a701d24a402575675c84683c97b289/119432903a13102eb21cb92d92013a1.jpg) 

        ![](https://gitlab.com/pic-01/pic-liu/uploads/d26f152ef1c8976367073ddbf27f7fdc/20200421162348.png)
    * ### Solution
      * #### change speed
        ![](https://gitlab.com/pic-01/pic-liu/uploads/4e6969db34338588c8c3b9651a10ee91/20200421162455.png)

    * ### Question 2
      * #### material overflow 
        ![](https://gitlab.com/pic-01/pic-liu/uploads/d95296d0e2a1c8d4a7b78388d1322e3c/20200421162913.png)
    * ### Solution
      * #### Remove the old nozzle
        ![](https://gitlab.com/pic-01/pic-liu/uploads/cc5e5c73caed3dcaa87c1814eee37628/1e1607b9253fffe37449539e4f0610e.jpg)
      * #### Composition of nozzle  
        ![](https://gitlab.com/pic-01/pic-liu/uploads/4e2fbaaa4fb659b20e5d53f840885b11/caeb4488c2f6657dcf77bf32b756d01.jpg)
      * #### As far as possible, keep the wires in the red section from touching the edges
        ![](https://gitlab.com/pic-01/pic-liu/uploads/604ed2eefa868d1ffeb8f3998d5a60ce/20200421164428.png)
        * #### Fasten
        ![](https://gitlab.com/pic-01/pic-liu/uploads/599c2146d6584cdaa1c81b971fbb5b2e/20200421165407.png)

        ![](https://gitlab.com/pic-01/pic-liu/uploads/f303a77fbabec5943671eac5f4da5737/4.gif)
        